package br.m7video.gr.DAO;

import java.util.List;

public interface DAO <T>{
	public void insert(T novo);
	public void delete(int id);
	public void update(T novo);
	public T get(int id);
	public List<T> getList();
	
}
