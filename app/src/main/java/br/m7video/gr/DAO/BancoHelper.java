package br.m7video.gr.DAO;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class BancoHelper extends SQLiteOpenHelper{
	
	private static final int VERSAO = 11;

	public BancoHelper(Context context) {
		super(context, "myporn", null, VERSAO);
		
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		
		StringBuilder sb = new StringBuilder();
		StringBuilder sb2 = new StringBuilder();

		sb.append("CREATE TABLE [usuario] ([id] INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, [login] VARCHAR NOT NULL, [resposta] VARCHAR NOT NULL);");
		
		db.execSQL(sb.toString());
		
		sb2.append("CREATE TABLE [video] ([id] INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT , [titulo] VARCHAR NOT NULL, [url] VARCHAR NOT NULL, [descricao] VARCHAR);");
		db.execSQL(sb2.toString());
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		try{
            db.execSQL("drop table video");
            db.execSQL("drop table usuario");
        }
        catch (Exception e){
            System.out.println("errro ao criar");
        }

		onCreate(db);
		
	}

}

