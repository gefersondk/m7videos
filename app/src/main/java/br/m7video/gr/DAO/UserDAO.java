package br.m7video.gr.DAO;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.widget.Toast;
import br.m7video.gr.model.Usuario;

public class UserDAO implements DAO<Usuario>{
	
	private SQLiteDatabase banco;
	private static final String TABELA_REGISTRO = "registro";
	private static final String TABELA_USUARIO = "Usuario";
	
	public UserDAO(Context context) {
		banco = new BancoHelper(context).getWritableDatabase();
	}
	
	@Override
	public void insert(Usuario novo) {
		ContentValues cv1 = new ContentValues();
		cv1.put("login", novo.getLogin());
        cv1.put("resposta", novo.getResposta());
		//cv1.put("longitude", novo.getPonto().getLongitude());
		this.banco.insert(TABELA_USUARIO, null, cv1);
		
		//ContentValues cv = new ContentValues();
		//cv.put("titulo", novo.getTitulo());
		//cv.put("descricao", novo.getDescricao());
		//cv.put("data", novo.getDataFormatada());
		//banco.insert(TABELA_REGISTRO, null, cv);
		
	}

	@Override
	public void delete(int id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(Usuario novo) {
		// TODO Auto-generated method stub
		
	}
	
	public boolean existUser(String login){
		String sql = "select login from usuario where login like ?;";
		//banco.rawQuery(sql, new String[]{});
		Cursor c=banco.rawQuery(sql,new String[]{login});
		Log.d("teste", c.getCount()+"");
		if(c.getCount() <= 0){
			return false;
		}

		return true;
		
	}

	@Override
	public Usuario get(int id) {
		// TODO Auto-generated method stub
		return null;
	}

    public Usuario get(String login){
        String sql = "select * from usuario where login like ?;";
        //banco.rawQuery(sql, new String[]{});
        Usuario usuario = new Usuario();
        Cursor c=banco.rawQuery(sql,new String[]{login});
        Log.d("teste", c.getCount()+"");
        if(c.getCount() <= 0){
            return null;
        }

        return usuario;
    }

	@Override
	public List<Usuario> getList() {
		String []colunas = {"id","titulo","descricao"};
		String tabela = TABELA_REGISTRO;
		Cursor c = this.banco.query(tabela,colunas,null,null,null,null,"titulo");
		Usuario l;
		List<Usuario> lista = new ArrayList<Usuario>();
		while(c.moveToNext()){
			l = new Usuario();
			l.setId(c.getInt(c.getColumnIndex("id")));
			//l.setTitulo(c.getString(c.getColumnIndex("titulo")));
			//l.setDescricao(c.getString(c.getColumnIndex("descricao")));
			
			lista.add(l);
			
		}
		
		return lista;
	}
	
	public SQLiteDatabase getBanco(){
		return this.banco;
	}

}
