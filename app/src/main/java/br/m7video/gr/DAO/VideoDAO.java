package br.m7video.gr.DAO;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import br.m7video.gr.model.Video;

public class VideoDAO implements DAO<Video>{
	
	private SQLiteDatabase banco;
	private static final String TABELA_REGISTRO = "video";

	
	public VideoDAO(Context context) {
		banco = new BancoHelper(context).getWritableDatabase();
	}
	
	@Override
	public void insert(Video novo) {

		ContentValues cv = new ContentValues();
		cv.put("titulo", novo.getTitulo());
		cv.put("descricao", novo.getDescricao());
		cv.put("url", novo.getUrl());
		banco.insert(TABELA_REGISTRO, null, cv);

	}

	@Override
	public void delete(int id) {

		this.banco.delete(TABELA_REGISTRO, "id = ?", new String[]{Integer.toString(id)});
		
	}

	@Override
	public void update(Video novo) {

		ContentValues cv = new ContentValues();
		cv.put("titulo", novo.getTitulo());
		cv.put("descricao", novo.getDescricao());
		cv.put("url", novo.getUrl());
		this.banco.update(TABELA_REGISTRO, cv, "id = ?", new String[]{Integer.toString(novo.getId())});
		
	}

	@Override
	public Video get(int id) {
		
		String []colunas = {"id","titulo","descricao","url"};
		String tabela = TABELA_REGISTRO;
		Cursor c = this.banco.query(tabela,colunas,"id = ?", new String[]{Integer.toString(id)},null,null,null);
        Video l;

		if(c.moveToNext()){

			l = new Video();
			l.setId(c.getInt(c.getColumnIndex("id")));
			l.setTitulo(c.getString(c.getColumnIndex("titulo")));
			l.setDescricao(c.getString(c.getColumnIndex("descricao")));
			l.setUrl(c.getString(c.getColumnIndex("url")));

			return l;
		}
		throw new RuntimeException(String.format("Video %d não existe", id));
	}

	@Override
	public List<Video> getList() {
		String []colunas = {"id","titulo","descricao","url"};
		String tabela = TABELA_REGISTRO;
		Cursor c = this.banco.query(tabela,colunas,null,null,null,null,"titulo");
		Video l;
		List<Video> lista = new ArrayList<Video>();
		while(c.moveToNext()){
			l = new Video();
			l.setId(c.getInt(c.getColumnIndex("id")));
			l.setTitulo(c.getString(c.getColumnIndex("titulo")));
			l.setDescricao(c.getString(c.getColumnIndex("descricao")));
			l.setUrl(c.getString(c.getColumnIndex("url")));
			
			lista.add(l);
			
		}
		
		return lista;
	}

	public SQLiteDatabase getBanco(){
		return this.banco;
	}

}
