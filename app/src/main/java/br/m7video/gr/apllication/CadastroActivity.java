package br.m7video.gr.apllication;

import br.m7video.gr.DAO.VideoDAO;
import br.m7video.gr.model.Video;
import br.myporn.gr.apllication.R;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class CadastroActivity extends Activity {
	
	private EditText titulo;
	private EditText descricao;
	private EditText url;
	private Button cancelar;
	private Button salvar;
	private Context context;
	private VideoDAO banco;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_cadastro);
		
		context = getApplicationContext();
		
		banco = new VideoDAO(context);
		
		titulo = (EditText) findViewById(R.id.editText1Cad);
		descricao = (EditText) findViewById(R.id.editText2Cad);
		url = (EditText) findViewById(R.id.editText3Cad);
		
		salvar = (Button) findViewById(R.id.button2Cad);
		salvar.setOnClickListener(new Salvar());
		
		cancelar =  (Button) findViewById(R.id.button1Cad);
		cancelar.setOnClickListener(new Cancelar());
	}

	

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	private class Cancelar implements OnClickListener{

		@Override
		public void onClick(View v) {
			Intent intent = new Intent(context, GrupoActivity.class);
			startActivity(intent);
			finish();
			
		}
		
	}
	
	private class Salvar implements OnClickListener{

		@Override
		public void onClick(View v) {
			try{
				Video conteudo = new Video();
				conteudo.setTitulo(titulo.getText().toString());
				conteudo.setDescricao(descricao.getText().toString());
				conteudo.setUrl(url.getText().toString());
				banco.insert(conteudo);
				Intent intent = new Intent(context, GrupoActivity.class);
				startActivity(intent);
				finish();
				
			}catch(Exception e){
				Log.d(null, e.getMessage());
			}
			
		}
		
	}
}
