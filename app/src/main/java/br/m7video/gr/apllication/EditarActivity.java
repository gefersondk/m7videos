package br.m7video.gr.apllication;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import br.m7video.gr.DAO.VideoDAO;
import br.m7video.gr.model.Video;
import br.myporn.gr.apllication.R;

public class EditarActivity extends Activity {
	
	private Button btnSalvar,btnCancelar;
	private TextView mensagem;
	private EditText edtTitulo,edtDescricao,edUrl;
	private VideoDAO banco;
	private Context context;
	private int id;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_editar);
		
		context = getApplicationContext();
		
		btnSalvar = (Button) findViewById(R.id.button2Edt);
		btnSalvar.setOnClickListener(new OnclickSalvar());
		btnCancelar = (Button) findViewById(R.id.button1Edt);
		btnCancelar.setOnClickListener(new Cancelar());
		
		edtTitulo = (EditText) findViewById(R.id.editText1Edt);
		edtDescricao = (EditText) findViewById(R.id.editText2Edt);
		edUrl = (EditText) findViewById(R.id.editText3Edt);
		
		
		
		banco = new VideoDAO(context);
		
		if(getIntent().hasExtra("id")){
			 Bundle extras = getIntent().getExtras();
			 int id = Integer.parseInt(extras.getString("id"));
			 Video conteudo = banco.get(id);
			 edtTitulo.setText(conteudo.getTitulo());
			 edtDescricao.setText(conteudo.getDescricao());
			 edUrl.setText(conteudo.getUrl());
			 edtTitulo.setSelection(edtTitulo.getText().length());
			 edtDescricao.setSelection(edtDescricao.getText().length());
			 edUrl.setSelection(edUrl.getText().length());
			 edtTitulo.setSelectAllOnFocus(true);
			 this.id = id;
			 
			 
		}
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.editar, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	private class OnclickSalvar implements OnClickListener{

		@Override
		public void onClick(View v) {
			 Video Video = banco.get(id);
			 Video.setTitulo(edtTitulo.getText().toString());
			 Video.setDescricao(edtDescricao.getText().toString());
			 Video.setUrl(edUrl.getText().toString());
			 banco.update(Video);
			 setResult(RESULT_OK);
			 finish();
			 
			
		}
		
	}
	
	private class Cancelar implements OnClickListener{

		@Override
		public void onClick(View v) {
			Intent intent = new Intent(context, GrupoActivity.class);
			startActivity(intent);
			finish();
			
		}
		
	}
}
