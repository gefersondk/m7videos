package br.m7video.gr.apllication;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import br.m7video.gr.DAO.UserDAO;
import br.myporn.gr.apllication.R;

public class MainActivity extends Activity {
	
	private EditText login;
	private Button logar;
	private Context context;
	private UserDAO banco;
    private Button edtSenha;
    private Button criarUsuario;
	
	public MainActivity() {
		
		
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login);
		
		
		context = getApplicationContext();
		banco = new UserDAO(context);
		/*UserDAO u = new UserDAO(context);
		Usuario lindo = new Usuario();
		lindo.setLogin("bayo123");
		u.insert(lindo);*/

        edtSenha = (Button) findViewById(R.id.buttonEdtSenha);

        criarUsuario = (Button)findViewById(R.id.buttoncriarUsuario);
        criarUsuario.setOnClickListener(new CriarUsuario());


		this.login = (EditText) findViewById(R.id.etPass);

		this.logar = (Button) findViewById(R.id.button1);
		this.logar.setOnClickListener(new Logar());
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	private class Logar implements OnClickListener{

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub

			if(banco.existUser(login.getText().toString())){
				Intent intent = new Intent(context, GrupoActivity.class);
				startActivity(intent);
				finish();
			}
			else{
				Toast.makeText(context, "Login invalido!", Toast.LENGTH_SHORT).show();
			}
			
		}
		
	}


    private class CriarUsuario implements OnClickListener{

        @Override
        public void onClick(View view) {
            try{
                Intent intent = new Intent(context, RegistrarUsuario.class);
                startActivity(intent);
            }
            catch (Exception e){

            }

            Toast.makeText(context, "pegou",Toast.LENGTH_LONG).show();
        }
    }
}
