package br.m7video.gr.apllication;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import br.m7video.gr.DAO.UserDAO;
import br.m7video.gr.model.Usuario;
import br.myporn.gr.apllication.R;

import org.w3c.dom.Text;

/**
 * Created by Jayce on 23/01/2015.
 */
public class RegistrarUsuario extends Activity{
    private EditText login;
    private EditText confLogin;
    private Button registrar;
    private UserDAO banco;
    private Context context;
    private EditText resposta;
    private TextView pergunta;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cadastro_conta);

        context = getApplicationContext();

        Spinner spinner = (Spinner) findViewById(R.id.spinner_pergunta);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(context, R.array.perguntas, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner.setAdapter(adapter);



        login = (EditText)findViewById(R.id.loginText);
        confLogin = (EditText)findViewById(R.id.confLoginText);
        registrar = (Button) findViewById(R.id.salvarUsuario);
        registrar.setOnClickListener(new Registrar());


        pergunta = (TextView)findViewById(R.id.perguntaView);
        resposta = (EditText)findViewById(R.id.respostaText);
    }

    private class Registrar implements View.OnClickListener{

        @Override
        public void onClick(View view) {
            try{

                Usuario usuario = new Usuario();

                usuario.setLogin(login.getText().toString());
                usuario.setResposta(resposta.getText().toString());
                Log.d(null,"paddo");

                banco = new UserDAO(context);
                banco.insert(usuario);
                finish();
            }
            catch(Exception e){
                Log.d(null,e.getMessage()+"");
            }
        }
    }
}
