package br.m7video.gr.apllication;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;
import br.m7video.gr.DAO.VideoDAO;
import br.m7video.gr.model.ListaVideoAdapter;
import br.m7video.gr.model.Video;
import br.myporn.gr.apllication.R;

public class VideosActivity extends Activity {

    private List<Video> conteudo;
    //private ArrayAdapter<Video> adapter;
    private BaseAdapter adapter;
    private ListView registros;
    private Context context;
    private Button voltar;
    private Button novo;
    private List<Video> listaDoAdapter;
    private VideoDAO banco;
    private final int MENU_CADASTRO = 0;
    private final int MENU_SAIR = 1;

    public VideosActivity() {
        this.conteudo = new ArrayList<Video>();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registros);


        context = getApplicationContext();

        banco = new VideoDAO(context);

        novo = (Button) findViewById(R.id.buttonNovo);
        novo.setOnClickListener(new Novo());
        listaDoAdapter = new ArrayList<Video>();
        listaDoAdapter.addAll(banco.getList());


        voltar = (Button) findViewById(R.id.sair);
        voltar.setOnClickListener(new Sair());



        this.registros = (ListView) findViewById(R.id.listView1);
        //adapter = new ArrayAdapter<Video>(this, android.R.layout.simple_list_item_1, conteudo);
        //this.registros.setAdapter(adapter);
        adapter = new ListaVideoAdapter(context, listaDoAdapter);
        this.registros.setAdapter(adapter);
        registros.setOnItemLongClickListener(new OnLongClick());
        registerForContextMenu(registros);

        registros.setOnItemClickListener(new Navegar());

        adapter.notifyDataSetChanged();


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.cadastro, menu);
        menu.add(0, MENU_CADASTRO, 0, "New");
        menu.add(0, MENU_SAIR, 0, "Exit");
        return true;
    }

    @Override
    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        // TODO Auto-generated method stub
        super.onMenuItemSelected(featureId, item);
        switch (item.getItemId()) {
            case MENU_CADASTRO:
                Intent intent = new Intent(context, CadastroActivity.class);
                startActivity(intent);
                finish();
                return true;

            case MENU_SAIR:
                finish();
                android.os.Process.killProcess(android.os.Process.myPid());
                break;

            default:
                break;
        }
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenuInfo menuInfo) {
        // TODO Auto-generated method stub
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_listview, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterContextMenuInfo info = (AdapterContextMenuInfo) item
                .getMenuInfo();
        Video dado = listaDoAdapter.get(info.position);
        switch (item.getItemId()) {

            case R.id.editar_id:
                Intent intent = new Intent(this,EditarActivity.class);
                int id2 = listaDoAdapter.get(info.position).getId();
                intent.putExtra("id", id2+"");
                //startActivity(intent2);
                startActivityForResult(intent, 2);

                return true;

            case R.id.delete_id:
                int id = listaDoAdapter.get(info.position).getId();
                banco.delete(id);

                listaDoAdapter.remove(info.position);
                adapter.notifyDataSetChanged();
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);


        if (resultCode == RESULT_OK) {
            if (requestCode == 2) {
                listaDoAdapter.clear();
                listaDoAdapter.addAll(this.banco.getList());
                adapter.notifyDataSetChanged();
            }
        }
    }

    private class Sair implements OnClickListener{

        @Override
        public void onClick(View v) {
            // TODO Auto-generated method stub
            //Intent intent = new Intent(context, MainActivity.class);
            //startActivity(intent);
            finish();
            android.os.Process.killProcess(android.os.Process.myPid());

        }

    }

    private class Novo implements OnClickListener{

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(context, CadastroActivity.class);
            startActivity(intent);
            finish();

        }

    }

    protected class OnLongClick implements OnItemLongClickListener {

        @Override
        public boolean onItemLongClick(AdapterView<?> parent, View view,
                                       int position, long id) {

            return false;
        }

    }

    private class Navegar implements OnItemClickListener{

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                                long id) {
            String link = listaDoAdapter.get(position).getUrl();

            try{
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(link));
                browserIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(browserIntent);
            }catch(Exception e){
                Toast.makeText(context, "Endereço do site invalido.", Toast.LENGTH_SHORT).show();
            }


        }


    }




}
