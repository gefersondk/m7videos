package br.m7video.gr.apllication;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;
import java.util.ArrayList;
import java.util.List;

import br.m7video.gr.model.Grupo;
import br.m7video.gr.model.ListaGrupoAdapter;
import br.m7video.gr.model.ListaVideoAdapter;
import br.myporn.gr.apllication.R;
import android.view.View.OnClickListener;

/**
 * Created by gefersonrd on 27/01/2015.
 */
public class GrupoActivity extends Activity {

    private ListaGrupoAdapter adapter;
    private Context context;
    private List<Grupo> grupos;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.grupo);

        ListView gruposView = (ListView) findViewById(R.id.grupos);

        context = getApplicationContext();

        Grupo a1 = new Grupo();
        Grupo a2 = new Grupo();
        Grupo a3 = new Grupo();

        grupos = new ArrayList<Grupo>();
        grupos.add(a1);
        grupos.add(a2);
        grupos.add(a3);
        adapter = new ListaGrupoAdapter(context, grupos);
        gruposView.setAdapter(adapter);
        gruposView.setOnItemClickListener(new OnclickGrupo());
    }

    protected class OnclickGrupo implements OnItemClickListener {


        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
            int idGrupo = grupos.get(position).getIdGrupo();
            Intent intent = new Intent(context, VideosActivity.class);
            intent.putExtra("idGrupo",idGrupo);
            startActivity(intent);

        }
    }






}
