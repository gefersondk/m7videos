package br.m7video.gr.model;

import java.util.Date;

/**
 * Created by gefersonrd on 27/01/2015.
 */
public class Grupo {

    private int idGrupo;
    private String nome;
    private Date dataInicio;

    public Grupo(){

    }

    public int getIdGrupo() {
        return idGrupo;
    }

    public void setIdGrupo(int idGrupo) {
        this.idGrupo = idGrupo;
    }

    public Date getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(Date dataInicio) {
        this.dataInicio = dataInicio;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String toString(){
        return "oi";
    }
}
