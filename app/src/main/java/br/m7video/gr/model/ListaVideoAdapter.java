package br.m7video.gr.model;

import java.util.List;

import android.R;
import android.R.color;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.WebView.FindListener;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemLongClickListener;

public class ListaVideoAdapter extends BaseAdapter{

    private Context context;
    private List<Video> lc;

    public ListaVideoAdapter(Context context, List<Video> conteudo) {
        this.context = context;
        this.lc = conteudo;
    }

    @Override
    public int getCount() {
        // pega tamanho da lista
        return lc.size();
    }

    @Override
    public Object getItem(int position) {
        // pega o int da posi��o que mandou
        return lc.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Video conteudo = lc.get(position);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(br.myporn.gr.apllication.R.layout.layout_adapter,null);
        //View layout = convertView;

        TextView titulo = (TextView)layout.findViewById(br.myporn.gr.apllication.R.id.titulo);
        titulo.setText(conteudo.getTitulo());
        titulo.setTextColor(Color.GREEN);

        TextView descricao = (TextView)layout.findViewById(br.myporn.gr.apllication.R.id.descricao);
        descricao.setText(conteudo.getDescricao());
        descricao.setTextColor(Color.BLUE);

        TextView url = (TextView) layout.findViewById(br.myporn.gr.apllication.R.id.url);
        url.setText(conteudo.getUrl());

        ImageView imgSite = (ImageView)layout.findViewById(br.myporn.gr.apllication.R.id.imgSite);
        imgSite.setImageResource(conteudo.getImagem());




        if(position%2==0){
            layout.setBackgroundColor(Color.parseColor("#4f4f4f"));
        }
//        else{
//        	
//        }




        //TextView titulo = layout.findViewById(R.id.text1);
        return layout;
    }


}
