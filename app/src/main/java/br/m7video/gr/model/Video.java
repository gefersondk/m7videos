package br.m7video.gr.model;

import android.R;

import java.util.HashMap;
import java.util.Map;

public class Video {

    private int id;
    private String titulo;
    private String url;
    private String descricao;
    private int grupo_id;

    private final Map<String,Integer> MAPIMG = new HashMap<String, Integer>(){{
        put("www.youtube.com",br.myporn.gr.apllication.R.drawable.youtube);
        put("vimeo.com", br.myporn.gr.apllication.R.drawable.vimeo);
        put("www.dailymotion.com", br.myporn.gr.apllication.R.drawable.dailymotion);

    }};

    public Video(){

    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getTitulo() {
        return titulo;
    }
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }
    public String getUrl() {
        return url;
    }
    public void setUrl(String url) {
        if(url.contains("http://")){
            this.url = url;
        }
        else{
            this.url = "http://"+url;
        }

    }
    public String getDescricao() {
        return descricao;
    }
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
    public int getImagem(){
        try{
            return MAPIMG.get(url.substring(7).split("/")[0]);
        }
        catch (Exception e){
            return br.myporn.gr.apllication.R.drawable.video;
        }
    }

    public int getGrupo_id() {
        return grupo_id;
    }

    public void setGrupo_id(int grupo_id) {
        this.grupo_id = grupo_id;
    }
}
